import javax.swing.*;

public class Exercise1 {

    public void question1(Account account2) {
        System.out.println("Chạy bài 1");
        // Kiểm tra account thứ 2
        //Nếu không có phòng ban (tức là department == null) thì sẽ in ra text
        //"Nhân viên này chưa có phòng ban"
        //Nếu không thì sẽ in ra text "Phòng ban của nhân viên này là ..."
        if (account2.department == null) {
            System.out.println("nhan vien nay chua co phong ban");
        } else {
            System.out.println("phong ban cua nhan vien nay la: " + account2.department.departmentName);
        }
    }

    public void question2(Account account2) {
        System.out.println("chay bai 2");
        int size = account2.groups.length;//=5
        if (size == 0) {
            System.out.println("Nhân viên này chưa có group");
        } else if (size == 1 || size == 2) {
            System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
        } else if (size > 3) {
            System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
        } else if (size == 4) {
            System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        } else {
            System.out.println("Truong hop khac");
        }

    }

    public void question3(Account account2) {
        System.out.println("chay bai 3");
        System.out.println((account2.department == null) ? "Nhan vien nay chua co phong ban"
                : "Phong ban cua nhan vien nay la: " + account2.department.departmentName);
    }

    public void question4(Account account1) {
        System.out.println("chay bai 4");
        System.out.println((account1.position.positionName == "DEV") ?
                "Day la Developer" : "Nguoi nay khong phai Developer");
    }

    //SWITCH CASE
    public void question5(Account[] accounts) {
        int size = accounts.length;
        System.out.println("chay bai 5");
        switch (size) {// size gtri can check  =5
            case 1:
                System.out.println("nhom co 1 thanh vien");
                break;
            case 2:
                System.out.println("nhom co 2 thanh vien");
                break;
            case 3:
                System.out.println("nhom co 3 thanh vien");
                break;
            default:
                System.out.println("nhom co nhieu thanh vien");
        }
    }

    public void question6(Group[] dsgroups1) {
        int size = dsgroups1.length;
        System.out.println("chay bai 6");
        switch (size) {
            case 1:
                System.out.println("Nhân viên này chưa có group");
                break;
            case 2:
                System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
                break;
            case 3:
                System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
                break;
            case 4:
                System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
        }
    }
    public void question7(Account account1){
        System.out.println("chay bai 7");
        switch (account1.position.positionName){
            case "DEV":
                System.out.println("Day la Developer");
                break;
            default:
                System.out.println("Nguoi nay khong phai Developer");
                break;
        }

    }
    public  void question8(Account[] accounts){
        System.out.println("chay bai 8");
        for (Account account: accounts
             ) {
            System.out.println("Email: " + account.email);
            System.out.println("FullName: " + account.fullName);
            System.out.println("Phong ban:" + account.department.departmentName);
        }
    }
    public void question9(Department[] departments){
        System.out.println("chay bai 9");
        for (Department department: departments
             ) {
            System.out.println("ID:" + department.departmentId);
            System.out.println("Name:" + department.departmentName);

        }
    }
    public void question10(Account[] accounts){
        System.out.println("chay bai 10");
        for (int i = 0; i < accounts.length ; i++) {
            System.out.println("Thong tin account thu " + (i + 1) + "la");
            System.out.println("Email:" + accounts[i].email );
            System.out.println("Fullname:" + accounts[i].fullName);
            System.out.println("Phong ban: " + accounts[i].department.departmentName);
        }
    }
    public void question11(Department[] departments){
        System.out.println("chay bai 11");
        for (int i = 0; i < departments.length; i++) {
            System.out.println("Thong tin department thu " + (i+1) + "la");
            System.out.println("ID: " + departments[i].departmentId);
            System.out.println("Name: " + departments[i].departmentName);
        }
    }
    public void question12(Department[] departments){
        System.out.println("chay bai 12");
        for (int i = 0; i < departments.length; i++) {
            System.out.println("Thong tin department thu " + (i+1) + "la");
            System.out.println("ID: " + departments[i].departmentId);
            System.out.println("Name: " + departments[i].departmentName);
        if (i == 1) {
            break;
        }

        }
    }
    public void question13(Account[] accounts) {
        System.out.println("chay bai 13");
        for (int i = 0; i < accounts.length; i++) {
            System.out.println("Thong tin account thu " + (i + 1) + "la");
            System.out.println("Email:" + accounts[i].email);
            System.out.println("Fullname:" + accounts[i].fullName);
            System.out.println("Phong ban: " + accounts[i].department.departmentName);
            if (i == 1) {
                continue;
            }
        }
    }
    public void question14(Account[] accounts) {
        System.out.println("chay bai 14");
        for (int i = 0; i < accounts.length; i++) {
            System.out.println("Thong tin account thu " + (i + 1) + "la");
            System.out.println("Email:" + accounts[i].email);
            System.out.println("Fullname:" + accounts[i].fullName);
            System.out.println("Phong ban: " + accounts[i].department.departmentName);
            if (accounts[i].accountId == 3) {
                break;
            }
        }
    }
    public void question15(){
        System.out.println("chay bai 15");
        for (int i = 2; i <= 20; i+=2) {
            System.out.println(i);
        }
    }
    public void question16(Account[] accounts){
        System.out.println("chay bai 16");
        int i = 0;
        while (i < accounts.length){
            System.out.println("Thong tin account thu " + (i + 1) + "la");
            System.out.println("Email:" + accounts[i].email );
            System.out.println("Fullname:" + accounts[i].fullName);
            System.out.println("Phong ban: " + accounts[i].department.departmentName);
            i++;
        }
    }


}



