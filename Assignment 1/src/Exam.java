import java.sql.Time;
import java.util.Date;

public class Exam {
    int examId;
    int code;
    String title;
    CategoryQuestion category;
    Time duration;
    Account creator;
    Date createDate;
}
